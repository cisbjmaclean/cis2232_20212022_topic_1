package info.hccis.sandbox20210916.bo;

import java.util.Scanner;

/**
 * Represents a student
 * @author bjmaclean
 * @since 20210916
 */
public class Student {
    
    private String name;
    private int studentId;
    private String program;

    public Student(){
        //default constructor
    }
    
    public Student(String name, int studentId, String program) {
        this.name = name;
        this.studentId = studentId;
        this.program = program;
    }

    public void getInformation(){
        System.out.println("Name:");
        Scanner input = new Scanner(System.in);
        name = input.nextLine();
        
        System.out.println("id:");
        studentId = input.nextInt();
        input.nextLine();
        
        System.out.println("Program:");
        program = input.nextLine();
        
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public void display(){
        System.out.println(this.toString());
    }
    
    public String toCSV(){
        return name+","+studentId+","+program;
    }
    
    @Override
    public String toString() {
        return "Student#"+studentId +System.lineSeparator()+ "Name:" + name+ System.lineSeparator() + "Program: " + program;
    }
    
    
    
}
