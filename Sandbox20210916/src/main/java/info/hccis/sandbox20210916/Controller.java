package info.hccis.sandbox20210916;

import com.google.gson.Gson;
import info.hccis.sandbox20210916.bo.Student;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Working with files.
 *
 * @author bjmaclean
 * @since 20210916
 */
public class Controller {

    private static final String FOLDER_NAME = "/cis2232/topic1/sample";
    private static final String FILE_NAME = "students.json";

    public static void main(String[] args) {
        System.out.println("Hello - File I/O");

        //Source:https://www.tabnine.com/code/java/methods/java.nio.file.Files/createDirectories
//        File file = new File(FOLDER_NAME);
//        boolean dirCreated = file.mkdir();

        String fullFileName = FOLDER_NAME+"\\"+FILE_NAME;
        try {
            Files.createDirectories(Paths.get(FOLDER_NAME));
        } catch (IOException ex) {
            System.out.println("Error creating directories");
        }

        ArrayList<String> linesFromFile = null;

        try {
            linesFromFile = (ArrayList) Files.readAllLines(Paths.get(fullFileName));
        } catch (IOException ex) {
            System.out.println("There was an exception reading the file");
        }

        //for each line, create a student object and add it to an ArrayList of Students
        ArrayList<Student> students = new ArrayList();

        boolean foundStudents = linesFromFile != null;

        Gson gson = new Gson();
        
        if (foundStudents) {
            System.out.println("Loading students from file");
            for (String current : linesFromFile) {
                if (current.length() > 1) {
                    System.out.println(current);
                    
                    Student student = gson.fromJson(current, Student.class);
                    
                    
//                    String[] parts = current.split(",");
//                    Student student = new Student(parts[0], Integer.parseInt(parts[1]), parts[2]);
                    students.add(student);
                }
            }
            System.out.println("Finished loading students...(added " + students.size() + " students to the list)");
        } else {
            System.out.println("No students to load.");
        }
        
        //show the students that were loaded.
        System.out.println("--------------------------------------------------------");
        System.out.println("- Here are the students that were loaded from the file.");
        System.out.println("--------------------------------------------------------");
        
        for(Student current: students){
            System.out.println(current.toString());
            System.out.println("");
        }
        
        System.out.println("--------------------------------------------------------");
        System.out.println("- There they were...continuing");
        System.out.println("--------------------------------------------------------");

        //Create a new Student
        Student student = new Student();
        student.getInformation();

        //Activity now is to write this new name to this file...
        FileWriter fileWriter;
        try {
            fileWriter = new FileWriter(fullFileName, true);
            fileWriter.write(gson.toJson(student) + System.lineSeparator());
            fileWriter.flush();
            //fileWriter.close();
        } catch (IOException ex) {
            System.out.println("Error writing...");
            ex.printStackTrace();
        }

        System.out.println("Thank-you for using the program.  ");
    }

}
